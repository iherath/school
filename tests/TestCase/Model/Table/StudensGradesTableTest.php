<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StudensGradesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StudensGradesTable Test Case
 */
class StudensGradesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StudensGradesTable
     */
    public $StudensGrades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.studens_grades',
        'app.student_grades',
        'app.students',
        'app.grades',
        'app.sub_grades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StudensGrades') ? [] : ['className' => 'App\Model\Table\StudensGradesTable'];
        $this->StudensGrades = TableRegistry::get('StudensGrades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StudensGrades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
