<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SudentSubjectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SudentSubjectsTable Test Case
 */
class SudentSubjectsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SudentSubjectsTable
     */
    public $SudentSubjects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sudent_subjects',
        'app.students',
        'app.studens_grades',
        'app.grades',
        'app.sub_grades',
        'app.subjects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SudentSubjects') ? [] : ['className' => 'App\Model\Table\SudentSubjectsTable'];
        $this->SudentSubjects = TableRegistry::get('SudentSubjects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SudentSubjects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
