<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SudentSubjectsFixture
 *
 */
class SudentSubjectsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'sudent_subject_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'student_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'subject_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'is_deleted' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'subject_student_student_fk_idx' => ['type' => 'index', 'columns' => ['student_id'], 'length' => []],
            'subject_student_subject_fk_idx' => ['type' => 'index', 'columns' => ['subject_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['sudent_subject_id'], 'length' => []],
            'subject_student_student_fk' => ['type' => 'foreign', 'columns' => ['student_id'], 'references' => ['students', 'student_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'subject_student_subject_fk' => ['type' => 'foreign', 'columns' => ['subject_id'], 'references' => ['subjects', 'subject_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'sudent_subject_id' => 1,
            'student_id' => 1,
            'subject_id' => 1,
            'created' => '2017-01-10 05:36:22',
            'modified' => '2017-01-10 05:36:22',
            'is_deleted' => 1
        ],
    ];
}
