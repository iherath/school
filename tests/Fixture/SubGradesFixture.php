<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SubGradesFixture
 *
 */
class SubGradesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'sub_grades_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'grade_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'student_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sub_grade' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'is_deleted' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'grade_subgrade_fk_idx' => ['type' => 'index', 'columns' => ['grade_id'], 'length' => []],
            'student_subgrade_fk_idx' => ['type' => 'index', 'columns' => ['student_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['sub_grades_id'], 'length' => []],
            'grade_subgrade_fk' => ['type' => 'foreign', 'columns' => ['grade_id'], 'references' => ['grades', 'grade_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'student_subgrade_fk' => ['type' => 'foreign', 'columns' => ['student_id'], 'references' => ['students', 'student_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'sub_grades_id' => 1,
            'grade_id' => 1,
            'student_id' => 1,
            'sub_grade' => 'Lorem ipsum dolor sit amet',
            'created' => '2016-09-19 17:27:09',
            'modified' => '2016-09-19 17:27:09',
            'is_deleted' => 1
        ],
    ];
}
