<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StudensGradesFixture
 *
 */
class StudensGradesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'student_grades_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'student_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'grade_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'is_deleted' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sub_grades_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'grade_gradestudents_fk_idx' => ['type' => 'index', 'columns' => ['grade_id'], 'length' => []],
            'student_gradestudents_fk_idx' => ['type' => 'index', 'columns' => ['student_id'], 'length' => []],
            'studentgrades_subgrades_id_idx' => ['type' => 'index', 'columns' => ['sub_grades_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['student_grades_id'], 'length' => []],
            'grade_gradestudents_fk' => ['type' => 'foreign', 'columns' => ['grade_id'], 'references' => ['grades', 'grade_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'student_gradestudents_fk' => ['type' => 'foreign', 'columns' => ['student_id'], 'references' => ['students', 'student_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'studentgrades_subgrades_id' => ['type' => 'foreign', 'columns' => ['sub_grades_id'], 'references' => ['sub_grades', 'sub_grades_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'student_grades_id' => 1,
            'student_id' => 1,
            'grade_id' => 1,
            'created' => '2016-09-19 17:24:03',
            'modified' => '2016-09-19 17:24:03',
            'is_deleted' => 1,
            'sub_grades_id' => 1
        ],
    ];
}
