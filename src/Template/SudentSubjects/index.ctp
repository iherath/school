<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sudent Subject'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sudentSubjects index large-9 medium-8 columns content">
    <h3><?= __('Sudent Subjects') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('sudent_subject_id') ?></th>
                <th><?= $this->Paginator->sort('student_id') ?></th>
                <th><?= $this->Paginator->sort('subject_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('is_deleted') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sudentSubjects as $sudentSubject): ?>
            <tr>
                <td><?= $this->Number->format($sudentSubject->sudent_subject_id) ?></td>
                <td><?= $sudentSubject->has('student') ? $this->Html->link($sudentSubject->student->student_id, ['controller' => 'Students', 'action' => 'view', $sudentSubject->student->student_id]) : '' ?></td>
                <td><?= $sudentSubject->has('subject') ? $this->Html->link($sudentSubject->subject->subject_id, ['controller' => 'Subjects', 'action' => 'view', $sudentSubject->subject->subject_id]) : '' ?></td>
                <td><?= h($sudentSubject->created) ?></td>
                <td><?= h($sudentSubject->modified) ?></td>
                <td><?= $this->Number->format($sudentSubject->is_deleted) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sudentSubject->sudent_subject_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sudentSubject->sudent_subject_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sudentSubject->sudent_subject_id], ['confirm' => __('Are you sure you want to delete # {0}?', $sudentSubject->sudent_subject_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
