<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sudent Subject'), ['action' => 'edit', $sudentSubject->sudent_subject_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sudent Subject'), ['action' => 'delete', $sudentSubject->sudent_subject_id], ['confirm' => __('Are you sure you want to delete # {0}?', $sudentSubject->sudent_subject_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sudent Subjects'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sudent Subject'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sudentSubjects view large-9 medium-8 columns content">
    <h3><?= h($sudentSubject->sudent_subject_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Student') ?></th>
            <td><?= $sudentSubject->has('student') ? $this->Html->link($sudentSubject->student->student_id, ['controller' => 'Students', 'action' => 'view', $sudentSubject->student->student_id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Subject') ?></th>
            <td><?= $sudentSubject->has('subject') ? $this->Html->link($sudentSubject->subject->subject_id, ['controller' => 'Subjects', 'action' => 'view', $sudentSubject->subject->subject_id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Sudent Subject Id') ?></th>
            <td><?= $this->Number->format($sudentSubject->sudent_subject_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Deleted') ?></th>
            <td><?= $this->Number->format($sudentSubject->is_deleted) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($sudentSubject->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($sudentSubject->modified) ?></td>
        </tr>
    </table>
</div>
