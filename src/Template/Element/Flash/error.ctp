<div class="alert alert-danger">
<button type="button" class="close" data-dismiss="alert">×</button>
<i class="fa fa-ban-circle"></i><strong>Oh snap!</strong> <a href="#" class="alert-link"><?= h($message) ?>
</div>