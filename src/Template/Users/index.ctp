<section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Users</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Users</h3>
              </div>
              <?= $this->Flash->render() ?> 
              <section class="panel panel-default">
                <header class="panel-heading">
                  DataTables 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
                <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                       
                        <th><?= $this->Paginator->sort('user_type_id') ?></th>
                        <th><?= $this->Paginator->sort('username') ?></th>
                         <th><?= $this->Paginator->sort('name') ?></th>
                        <th><?= $this->Paginator->sort('gender') ?></th>
                        <th><?= $this->Paginator->sort('mobile') ?></th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user): ?>
                        <tr>
                          
                            <td><?= h($user->user_type->user_types) ?></td>
                            <td><?= h($user->username) ?></td>
                            <td><?= h($user->title.' '.$user->first_name.' '.$user->last_name) ?></td>
                
                            <td><?= ($user->gender==1)?'Male':'Female'; ?></td>
                            <td><?= h($user->mobile) ?></td>
                            <td class="actions">

                            <?php echo $this->Html->link($this->Html->tag('span','',array('class'=>'fa fa-eye')), array('action' => 'view', $user->user_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'View This User','class'=>'btn btn-info btn-icon btn-sm')); ?>
                            
                             <?php echo $this->Html->link($this->Html->tag('span','',array('class'=>'fa fa-edit')), array('action' => 'Edit', $user->user_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Edit This User','class'=>'btn btn-warning btn-icon btn-sm')); ?>

                             <?php echo $this->Form->postLink($this->Html->tag('span','',array('class'=>'fa fa-trash-o')), array('action' => 'delete', $user->user_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Delete This User','class'=>'btn btn-danger btn-icon btn-sm','confirm' => __('Are you sure you want to delete this student?',$user->user_id))); ?>
                             
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
          </section>

    <div class="paginator" align="right">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
