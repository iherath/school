

<?= $this->Form->create(null,['id'=>'bootstrap-validator-form','role'=>'form','class'=>'panel-body wrapper-lg']) ?>
 <?= $this->Flash->render() ?> 
  
      <div class="form-group">
        <label class="control-label">Email</label>
        
         <input type="text" class="form-control input-lg" name="username" required="required" placeholder="Username">
      </div>
      <div class="form-group">
        <label class="control-label">Password</label>
        <input type="password" class="form-control input-lg" name="password" required="required" placeholder="Password">
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="remember_me" value="0"> Keep me logged in
        </label>
      </div>
      <a href="#" class="pull-right m-t-xs"><small>Forgot password?</small></a>
      <button type="submit" class="btn btn-primary">Sign in</button>
      <!-- <div class="line line-dashed"></div>
      <a href="#" class="btn btn-facebook btn-block m-b-sm"><i class="fa fa-facebook pull-left"></i>Sign in with Facebook</a>
      <a href="#" class="btn btn-twitter btn-block"><i class="fa fa-twitter pull-left"></i>Sign in with Twitter</a>
      <div class="line line-dashed"></div>
      <p class="text-muted text-center"><small>Do not have an account?</small></p>
      <a href="signup.html" class="btn btn-default btn-block">Create an account</a> -->
<?= $this->Form->end() ?>