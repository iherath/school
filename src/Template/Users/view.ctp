<section class="vbox">
    <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Users</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Users</h3>
              </div>
              <section class="panel panel-default">
                <header class="panel-heading">
                  DataTables 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
            <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th><?= $this->Paginator->sort('User Type') ?></th>
                        <th><?= $this->Paginator->sort('Title') ?></th>
                        <th><?= $this->Paginator->sort('First Name') ?></th>
                        <th><?= $this->Paginator->sort('Last Name') ?></th>
                        <th><?= $this->Paginator->sort('Mobile') ?></th>
                        <th><?= $this->Paginator->sort('Gender') ?></th>
                        <th><?= $this->Paginator->sort('User ID') ?></th>
                        <th><?= $this->Paginator->sort('Date Of Birth') ?></th>
                        <th><?= $this->Paginator->sort('Address') ?></th>
                      </tr>
                    </thead>
                    <tbody>
                  
                        <tr>
                            <td><?= h($user->user_type_id) ?></td>
                            <td><?= h($user->title) ?></td>
                            <td><?= h($user->first_name) ?></td>
                            <td><?= h($user->last_name) ?></td>
                            <td><?= h($user->mobile) ?></td>
                            <td><?= h($user->gender) ?></td>
                            <td><?= h($user->user_id) ?></td>
                            <td><?= h($user->date_of_birth) ?></td>
                            <td><?= h($user->address) ?></td>
                        
                        </tr>
                  
                    </tbody>
                  </table>
            </div>
    </section>
  </section>
</section>