<section class="vbox">
<section class="scrollable padder">
        <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Users</a></li>
        </ul>

        <div class="m-b-md">
            <h3 class="m-b-none">Users</h3>
        </div>

<section class="panel panel-default">
    <header class="panel-heading">
    <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
    </header>

<div class="row">
    <div class="col-sm-12">
        <section class="panel panel-default">
                <header class="panel-heading font-bold">
                     Update this user
                </header>
        </section>
        <div class="panel-body">
               <?= $this->Form->create($user,['enctype'=>'multipart/form-data']) ?>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">User Type</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('user_type_id',['options' => $userTypes,'class'=>'form-control','label'=>false]);?>
                        </div>
                    <br/><br/>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Title</label>
                        <div class="col-sm-10">
                            <?php echo $this->Form->input('title', ['options'=>['Mr.'=>'Mr','Ms.'=>'Ms','Mrs.'=>'Mrs','Dr.'=>'Dr','Prof.'=>'Prof','Capt'=>'Capt'], 'empty' => 'Title','class'=>'form-control','label'=>false ,'required'=>'required','data-error'=>'Please select title']);?>
                        </div>
                    <br/><br/>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">First Name</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('first_name',['class'=>'form-control','label'=>false]);?>
                        </div>
                    <br/><br/>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Last Name</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('last_name',['class'=>'form-control','label'=>false]);?>
                        </div>
                    <br/><br/>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Mobile</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('mobile',['class'=>'form-control','label'=>false]);?>
                        </div>
                    <br/><br/>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Land Line</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('land_line',['class'=>'form-control','label'=>false]);?>
                        </div>
                    <br/><br/>
                </div>

                                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Date Of Birth</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('date_of_birth',['class'=>'form-control datepicker-input','type'=>'text','data-date-format'=>'yyyy-mm-dd','label'=>false]);?>
                        </div>
                    <br/><br/>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Gender</label>
                        <div class="col-sm-10">
                            <?php echo $this->Form->input('gender', ['options'=>['1'=>'Male','2'=>'Female'], 'empty' => 'Gender','class'=>'form-control','label'=>false ,'required'=>'required','data-error'=>'Please select gender']);?>
                        </div>
                    <br/><br/>
                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Address</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('address',['class'=>'form-control','label'=>false]);?>
                             <br/>
                        </div>
                    <br/><br/>
                </div>
                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Username</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('username',['class'=>'form-control','label'=>false]);?>
                              <br/>
                        </div>
                    <br/><br/>
                </div>
              
                <div class="form-group">
                           <label class="col-md-2 control-label">Profile Picture(Optional)</label>
                            <div class="col-md-10">
                                <?php  echo $this->Form->input('profile_pic',['type'=>'file','class'=>'form-control','label'=>false ]);?>
                                <span class="file-custom"></span> 
                                </br>    
                            </div>
                        </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1"></label>
                        <div class="col-sm-10">
                        <br/> <br/>
                            <button type="submit" class="btn btn-s-md btn-primary">Update</button>
                        </div>
                </div>
                <?= $this->Form->end() ?>
        </div>

    </div>
</div>
</section>
</section>
</section>

