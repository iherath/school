<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Forgot Password Request'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="forgotPasswordRequest form large-9 medium-8 columns content">
    <?= $this->Form->create($forgotPasswordRequest) ?>
    <fieldset>
        <legend><?= __('Add Forgot Password Request') ?></legend>
        <?php
            echo $this->Form->input('user_accesss_domain');
            echo $this->Form->input('email');
            echo $this->Form->input('date_requested');
            echo $this->Form->input('date_time', ['empty' => true]);
            echo $this->Form->input('request_code');
            echo $this->Form->input('is_expired');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
