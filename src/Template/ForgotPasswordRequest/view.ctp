<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Forgot Password Request'), ['action' => 'edit', $forgotPasswordRequest->forgot_password_request_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Forgot Password Request'), ['action' => 'delete', $forgotPasswordRequest->forgot_password_request_id], ['confirm' => __('Are you sure you want to delete # {0}?', $forgotPasswordRequest->forgot_password_request_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Forgot Password Request'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Forgot Password Request'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="forgotPasswordRequest view large-9 medium-8 columns content">
    <h3><?= h($forgotPasswordRequest->forgot_password_request_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($forgotPasswordRequest->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Forgot Password Request Id') ?></th>
            <td><?= $this->Number->format($forgotPasswordRequest->forgot_password_request_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Expired') ?></th>
            <td><?= $this->Number->format($forgotPasswordRequest->is_expired) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Requested') ?></th>
            <td><?= h($forgotPasswordRequest->date_requested) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Time') ?></th>
            <td><?= h($forgotPasswordRequest->date_time) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($forgotPasswordRequest->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($forgotPasswordRequest->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('User Accesss Domain') ?></h4>
        <?= $this->Text->autoParagraph(h($forgotPasswordRequest->user_accesss_domain)); ?>
    </div>
    <div class="row">
        <h4><?= __('Request Code') ?></h4>
        <?= $this->Text->autoParagraph(h($forgotPasswordRequest->request_code)); ?>
    </div>
</div>
