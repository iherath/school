<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Forgot Password Request'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="forgotPasswordRequest index large-9 medium-8 columns content">
    <h3><?= __('Forgot Password Request') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('forgot_password_request_id') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('date_requested') ?></th>
                <th><?= $this->Paginator->sort('date_time') ?></th>
                <th><?= $this->Paginator->sort('is_expired') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($forgotPasswordRequest as $forgotPasswordRequest): ?>
            <tr>
                <td><?= $this->Number->format($forgotPasswordRequest->forgot_password_request_id) ?></td>
                <td><?= h($forgotPasswordRequest->email) ?></td>
                <td><?= h($forgotPasswordRequest->date_requested) ?></td>
                <td><?= h($forgotPasswordRequest->date_time) ?></td>
                <td><?= $this->Number->format($forgotPasswordRequest->is_expired) ?></td>
                <td><?= h($forgotPasswordRequest->created) ?></td>
                <td><?= h($forgotPasswordRequest->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $forgotPasswordRequest->forgot_password_request_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $forgotPasswordRequest->forgot_password_request_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $forgotPasswordRequest->forgot_password_request_id], ['confirm' => __('Are you sure you want to delete # {0}?', $forgotPasswordRequest->forgot_password_request_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
