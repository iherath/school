      <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Grades</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Grades</h3>
              </div>
               <?= $this->Flash->render() ?> 
              <section class="panel panel-default">
                <header class="panel-heading">
                  All Grades 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
                <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                     
                        <th><?= $this->Paginator->sort('grade') ?></th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($grades as $grade): ?>
                        <tr>
                           
                            <td><?= h($grade->grade) ?></td>
                            <td class="actions">
                            

                                <?php echo $this->Html->link($this->Html->tag('span','',array('class'=>'fa fa-edit')), array('action' => 'Edit', $grade->grade_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Edit This Grade','class'=>'btn btn-warning btn-icon btn-sm')); ?>

                                <?php echo $this->Form->postLink($this->Html->tag('span','',array('class'=>'fa fa-trash-o')), array('action' => 'delete', $grade->grade_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Delete This Grade','class'=>'btn btn-danger btn-icon btn-sm','confirm' => __('Are you sure you want to delete grade',$grade->grade_id))); ?>

                                
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
          </section>

    <div class="paginator" align="right">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>