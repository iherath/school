 <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Grades</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Grades</h3>
              </div>
              <section class="panel panel-default">
                <header class="panel-heading">
                  DataTables 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
                <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th><?= $this->Paginator->sort('grade_id') ?></th>
                        <th><?= $this->Paginator->sort('grade') ?></th>
                        
                      </tr>
                    </thead>
                    <tbody>
                  
                        <tr>
                            <td><?= h($grade->grade_id) ?></td>
                            <td><?= h($grade->grade) ?></td>
                                           
                        
                        </tr>
                  
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
          </section>

