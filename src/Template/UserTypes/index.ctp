<section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Users Types</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">User Types</h3>
              </div>
              <?= $this->Flash->render() ?> 
              <section class="panel panel-default">
                <header class="panel-heading">
                  DataTables 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
                <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th><?= $this->Paginator->sort('user_types') ?></th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($userTypes as $userType): ?>
                        <tr>
                            <td><?= h($userType->user_types) ?></td>
                
                            <td class="actions">
                            
                             <?php echo $this->Html->link($this->Html->tag('span','',array('class'=>'fa fa-edit')), array('action' => 'Edit', $userType->user_type_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Edit This User Type','class'=>'btn btn-warning btn-icon btn-sm')); ?>

                             <?php echo $this->Form->postLink($this->Html->tag('span','',array('class'=>'fa fa-trash-o')), array('action' => 'delete', $userType->user_type_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Delete This User Type','class'=>'btn btn-danger btn-icon btn-sm','confirm' => __('Are you sure you want to delete this student?',$userType->user_type_id))); ?>
                             
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
          </section>

    <div class="paginator" align="right">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
