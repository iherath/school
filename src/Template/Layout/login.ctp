
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// $cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
    <title>
       
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css(['bootstrap','animate','font-awesome.min','font','bootstrap_calendar','app','calendar/bootstrap_calendar','../js/datepicker/datepicker.css']) ?>
   

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
 <section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
    <div class="container aside-xxl">
      <!-- <a class="navbar-brand block" href="index.html">Hedunu Wewa Central College</a> -->
     <div class="login-logo">
      <img src="<?= APP_URL ?>img/logo.png">
     </div>
      <section class="panel panel-default bg-white m-t-lg">
        <header class="panel-heading text-center">
          <strong>Sign in</strong>
        </header>
        <?= $this->fetch('content') ?>


      </section>
    </div>
  </section>
  <script src="js/jquery.min.js"></script>
   <?= $this->Html->script('jquery.min') ?>
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="http://code.highcharts.com/modules/exporting.js"></script>
  <!-- Bootstrap -->
   <?= $this->Html->script(['bootstrap','app','app.plugin','slimscroll/jquery.slimscroll.min','charts/easypiechart/jquery.easy-pie-chart','charts/sparkline/jquery.sparkline.min','charts/flot/jquery.flot.min','charts/flot/jquery.flot.tooltip.min','charts/flot/jquery.flot.resize','charts/flot/jquery.flot.grow','charts/flot/demo','calendar/bootstrap_calendar','calendar/demo','sortable/jquery.sortable','datepicker/bootstrap-datepicker']) ?>
  
  
  <script type="text/javascript">

    $(function () {
        $('#flot-1ine2').highcharts({
            title: {
                text: 'Monthly Average Temperature',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: WorldClimate.com',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: '°C'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Tokyo',
                data: [7.0, 6.9, 9.5, 14.5, 40.2, 21.5, 25.2, 50.5, 23.3, 18.3, 13.9, 9.6]
            }]
        });
    });

  </script>

</body>
</html>
