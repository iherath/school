<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Studens Grade'), ['action' => 'edit', $studensGrade->student_grades_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Studens Grade'), ['action' => 'delete', $studensGrade->student_grades_id], ['confirm' => __('Are you sure you want to delete # {0}?', $studensGrade->student_grades_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Studens Grades'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Studens Grade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Grades'), ['controller' => 'Grades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Grade'), ['controller' => 'Grades', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sub Grades'), ['controller' => 'SubGrades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sub Grade'), ['controller' => 'SubGrades', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="studensGrades view large-9 medium-8 columns content">
    <h3><?= h($studensGrade->student_grades_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Student') ?></th>
            <td><?= $studensGrade->has('student') ? $this->Html->link($studensGrade->student->student_id, ['controller' => 'Students', 'action' => 'view', $studensGrade->student->student_id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Grade') ?></th>
            <td><?= $studensGrade->has('grade') ? $this->Html->link($studensGrade->grade->grade_id, ['controller' => 'Grades', 'action' => 'view', $studensGrade->grade->grade_id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Sub Grade') ?></th>
            <td><?= $studensGrade->has('sub_grade') ? $this->Html->link($studensGrade->sub_grade->sub_grades_id, ['controller' => 'SubGrades', 'action' => 'view', $studensGrade->sub_grade->sub_grades_id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Studens Gradescol') ?></th>
            <td><?= h($studensGrade->studens_gradescol) ?></td>
        </tr>
        <tr>
            <th><?= __('Student Grades Id') ?></th>
            <td><?= $this->Number->format($studensGrade->student_grades_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Deleted') ?></th>
            <td><?= $this->Number->format($studensGrade->is_deleted) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($studensGrade->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($studensGrade->modified) ?></td>
        </tr>
    </table>
</div>
