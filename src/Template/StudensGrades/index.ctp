<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Studens Grade'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Grades'), ['controller' => 'Grades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Grade'), ['controller' => 'Grades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sub Grades'), ['controller' => 'SubGrades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sub Grade'), ['controller' => 'SubGrades', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="studensGrades index large-9 medium-8 columns content">
    <h3><?= __('Studens Grades') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('student_grades_id') ?></th>
                <th><?= $this->Paginator->sort('student_id') ?></th>
                <th><?= $this->Paginator->sort('grade_id') ?></th>
                <th><?= $this->Paginator->sort('sub_grades_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('is_deleted') ?></th>
                <th><?= $this->Paginator->sort('studens_gradescol') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($studensGrades as $studensGrade): ?>
            <tr>
                <td><?= $this->Number->format($studensGrade->student_grades_id) ?></td>
                <td><?= $studensGrade->has('student') ? $this->Html->link($studensGrade->student->student_id, ['controller' => 'Students', 'action' => 'view', $studensGrade->student->student_id]) : '' ?></td>
                <td><?= $studensGrade->has('grade') ? $this->Html->link($studensGrade->grade->grade_id, ['controller' => 'Grades', 'action' => 'view', $studensGrade->grade->grade_id]) : '' ?></td>
                <td><?= $studensGrade->has('sub_grade') ? $this->Html->link($studensGrade->sub_grade->sub_grades_id, ['controller' => 'SubGrades', 'action' => 'view', $studensGrade->sub_grade->sub_grades_id]) : '' ?></td>
                <td><?= h($studensGrade->created) ?></td>
                <td><?= h($studensGrade->modified) ?></td>
                <td><?= $this->Number->format($studensGrade->is_deleted) ?></td>
                <td><?= h($studensGrade->studens_gradescol) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $studensGrade->student_grades_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $studensGrade->student_grades_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $studensGrade->student_grades_id], ['confirm' => __('Are you sure you want to delete # {0}?', $studensGrade->student_grades_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
