<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Studens Grades'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Grades'), ['controller' => 'Grades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Grade'), ['controller' => 'Grades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sub Grades'), ['controller' => 'SubGrades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sub Grade'), ['controller' => 'SubGrades', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="studensGrades form large-9 medium-8 columns content">
    <?= $this->Form->create($studensGrade) ?>
    <fieldset>
        <legend><?= __('Add Studens Grade') ?></legend>
        <?php
            echo $this->Form->input('student_id', ['options' => $students, 'empty' => true]);
            echo $this->Form->input('grade_id', ['options' => $grades, 'empty' => true]);
            echo $this->Form->input('sub_grades_id', ['options' => $subGrades, 'empty' => true]);
            echo $this->Form->input('is_deleted');
            echo $this->Form->input('studens_gradescol');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
