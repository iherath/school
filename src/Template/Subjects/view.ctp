<!-- <!-- <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Subject'), ['action' => 'edit', $subject->subject_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Subject'), ['action' => 'delete', $subject->subject_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subject->subject_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Subjects'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subject'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subjects view large-9 medium-8 columns content">
    <h3><?= h($subject->subject_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Student') ?></th>
            <td><?= $subject->has('student') ? $this->Html->link($subject->student->student_id, ['controller' => 'Students', 'action' => 'view', $subject->student->student_id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Subject Name') ?></th>
            <td><?= h($subject->subject_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Subject Id') ?></th>
            <td><?= $this->Number->format($subject->subject_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Deleted') ?></th>
            <td><?= $this->Number->format($subject->is_deleted) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($subject->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($subject->modified) ?></td>
        </tr>
    </table>
</div>
-----------------------------------------------------------------------------  -->

 <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Subjects</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Subjects</h3>
              </div>
              <section class="panel panel-default">
                <header class="panel-heading">
                  DataTables 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
                <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th><?= $this->Paginator->sort('student_id') ?></th>
                        <th><?= $this->Paginator->sort('subject_id') ?></th>
                        <th><?= $this->Paginator->sort('subject_name') ?></th>
                      </tr>
                    </thead>
                    <tbody>
                  
                        <tr>
                            <td><?= h($subject->student_id) ?></td>
                            <td><?= h($subject->subject_id) ?></td>
                            <td><?= h($subject->subject_name) ?></td>                         
                        
                        </tr>
                  
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
          </section>