<section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Sub Grades</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Sub Grades</h3>
              </div>
               <?= $this->Flash->render() ?> 
              <section class="panel panel-default">
                <header class="panel-heading">
                  All sub grades 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
                <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                     
                     
                        <th><?= $this->Paginator->sort('grade_id') ?></th>
                        <th><?= $this->Paginator->sort('sub_grade') ?></th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($subGrades as $subGrade): ?>
                        <tr>
                           
                            <td><?= h($subGrade->grade->grade) ?></td>
                            <td><?= h($subGrade->sub_grade) ?></td>
                            <td class="actions">
                            

                            <?php echo $this->Html->link($this->Html->tag('span','',array('class'=>'fa fa-edit')), array('action' => 'Edit', $subGrade->sub_grades_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Edit This Sub Grade','class'=>'btn btn-warning btn-icon btn-sm')); ?>

                            <?php echo $this->Form->postLink($this->Html->tag('span','',array('class'=>'fa fa-trash-o')), array('action' => 'delete', $subGrade->sub_grades_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Delete This Sub Grade','class'=>'btn btn-danger btn-icon btn-sm','confirm' => __('Are you sure you want to delete grade',$subGrade->sub_grades_id))); ?>

                                
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
          </section>

    <div class="paginator" align="right">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
