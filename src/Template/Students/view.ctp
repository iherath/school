  
   <section id="content">
          <section class="vbox">
            <header class="header bg-white b-b b-light">
              <p><?= $student->first_name.' '.$student->last_name?>'s profile</p>
            </header>
            <section class="scrollable">
              <section class="hbox stretch">
                <aside class="aside-lg bg-light lter b-r">
                  <section class="vbox">
                    <section class="scrollable">
                      <div class="wrapper">
                        <div class="clearfix m-b">
                          <a href="#" class="pull-left thumb m-r">
                          <?php if(!empty($student->profile_pic)){?>
                             <img src="<?= APP_URL?>student_pic/<?= $student->profile_pic?>" class="img-circle">
                           <?php }else{?>
                            <img src="<?= APP_URL?>student_pic/no-image.png" class="img-circle">
                           <?php }?> 
                          </a>
                          <div class="clear">
                            <div class="h4 m-t-xs m-b-xs"><?= $student->first_name.' '.$student->last_name?></div>
                            <div class="h5 m-t-xs m-b-xs"><i class="fa fa-registered"></i> <?= $student->reg_number?></div>
                            <small class="text-muted"><i class="fa fa-map-marker"></i> <?= $student->address?></small>
                          </div>                
                        </div>
                        <div class="panel wrapper panel-success">
                          <div class="row">
                            <div class="col-xs-12">
                              <a href="#">
                                <span class="m-b-xs h4 block">Grade - <?= $student->studens_grades[0]->grade->grade?> (<?= $student->studens_grades[0]->sub_grade->sub_grade?>)</span>
                                <small class="text-muted"><?=  $student->medium?></small>
                              </a>
                            </div>
                            
                          </div>
                        </div>
                        <div class="btn-group btn-group-justified m-b">
                          <a class="btn btn-primary btn-rounded" data-toggle="button">
                            <span class="text">
                              <!-- <i class="fa fa-eye"></i> Follow -->
                            </span>
                            <span class="text-active">
                              <!-- <i class="fa fa-eye-slash"></i> Following -->
                            </span>
                          </a>
                          <a class="btn btn-dark btn-rounded" data-loading-text="Connecting">
                            <!-- <i class="fa fa-comment-o"></i> Chat -->
                          </a>
                        </div>
                        <div>
                        
                          <small class="text-uc text-xs text-muted">Description</small>
                          <p><?= $student->description ?></p>
                          <div class="line"></div>
                          <small class="text-uc text-xs text-muted">connection</small>
                          <p class="m-t-sm">
                            <a href="#" class="btn btn-rounded btn-twitter btn-icon"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="btn btn-rounded btn-facebook btn-icon"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="btn btn-rounded btn-gplus btn-icon"><i class="fa fa-google-plus"></i></a>
                          </p>
                        </div>
                      </div>
                    </section>
                  </section>
                </aside>
                <aside class="bg-white">
                  <section class="vbox">
                    <header class="header bg-light bg-gradient">
                      <ul class="nav nav-tabs nav-white">
                        <li class="active"><a href="#activity" data-toggle="tab">Basic Info</a></li>
                        <li class=""><a href="#subjects" data-toggle="tab">Subjects</a></li>
                        <li class=""><a href="#extra_activities" data-toggle="tab">Extra Activities</a></li>
                      </ul>
                    </header>
                    <section class="scrollable">
                      <div class="tab-content">
                        <div class="tab-pane active" id="activity">
                          <ul class="list-group no-radius m-b-none m-t-n-xxs list-group-lg no-border">
                            <li class="list-group-item">
                              
                              <a href="#" class="clear">
                                
                                <strong class="block">Date of Birth</strong>
                                <small><?= $student->date_of_birth->i18nFormat('yyyy-MM-dd')?> </small>
                              </a>
                            </li>
                            <li class="list-group-item">
                             
                              <a href="#" class="clear">
                                <strong class="block">Gender</strong>
                                <small><?= ($student->gender)?'Male':'Female'?></small>
                              </a>
                            </li>
                            <li class="list-group-item">
                             
                              <a href="#" class="clear">
                                <strong class="block">Contact Number</strong>
                                <small><?= $student->contact_number ?></small>
                              </a>
                            </li>
                            <li class="list-group-item">
                             
                              <a href="#" class="clear">
                                <strong class="block">Address</strong>
                                <small><?= $student->address?></small>
                              </a>
                            </li>
                            
                            
                           
                          </ul>
                        </div>
                        <div class="tab-pane" id="subjects">
                          <div class="text-center wrapper">
                              <table class="table table-striped m-b-none text-sm">
                                  <thead>
                                    <tr>
                                      <th>Subjects</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                
                                 <?php  foreach ($student->sudent_subjects as $sudent_subject) { ?>
                                    <tr>                    
                                      <td>
                                        <?= $sudent_subject->subject->subject_name?>
                                      </td>
                                    </tr>
                                  <?php }?>  

                                  </tbody>
                                </table>

                          </div>
                        </div>
                        <div class="tab-pane" id="extra_activities">
                          <div class="text-center wrapper">
                            <i class="fa fa-spinner fa fa-spin fa fa-large"></i>
                          </div>
                        </div>
                      </div>
                    </section>
                  </section>
                </aside>
                <aside class="col-lg-4 b-l">
                  <section class="vbox">
                    <section class="scrollable">
                      <div class="wrapper">
                        <section class="panel panel-default">
                          <form>
                            <textarea class="form-control no-border" rows="3" placeholder="Special note..."></textarea>
                          </form>
                          <footer class="panel-footer bg-light lter">
                            <button class="btn btn-info pull-right btn-sm">Save</button>
                            <ul class="nav nav-pills nav-sm">
                              <li><a href="#"><i class="fa fa-camera text-muted"></i></a></li>
                              <li><a href="#"><i class="fa fa-video-camera text-muted"></i></a></li>
                            </ul>
                          </footer>
                        </section>
                        <section class="panel panel-default">
                          <h4 class="font-thin padder">Special Note</h4>
                          <ul class="list-group">
                            <li class="list-group-item">
                                <p>Wellcome <a href="#" class="text-info">@Drew Wllon</a> and play this web application template, have fun1 </p>
                                <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 minuts ago</small>
                            </li>
                            <li class="list-group-item">
                                <p>Morbi nec <a href="#" class="text-info">@Jonathan George</a> nunc condimentum ipsum dolor sit amet, consectetur</p>
                                <small class="block text-muted"><i class="fa fa-clock-o"></i> 1 hour ago</small>
                            </li>
                            <li class="list-group-item">                     
                                <p><a href="#" class="text-info">@Josh Long</a> Vestibulum ullamcorper sodales nisi nec adipiscing elit. </p>
                                <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 hours ago</small>
                            </li>
                          </ul>
                        </section>
                        <section class="panel clearfix bg-info lter">
                          <div class="panel-body">
                            <a href="#" class="thumb pull-left m-r">
                              <!-- <img src="images/avatar.jpg" class="img-circle"> -->
                            </a>
                          <!--   <div class="clear">
                              <a href="#" class="text-info">@Mike Mcalidek <i class="fa fa-twitter"></i></a>
                              <small class="block text-muted">2,415 followers / 225 tweets</small>
                              <a href="#" class="btn btn-xs btn-success m-t-xs">Follow</a>
                            </div> -->
                          </div>
                        </section>
                      </div>
                    </section>
                  </section>              
                </aside>
              </section>
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
