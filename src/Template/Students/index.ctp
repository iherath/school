          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Students</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Students</h3>
              </div>
              <?= $this->Flash->render() ?> 
              <section class="panel panel-default">
                <header class="panel-heading">
                  All students 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
                <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th><?= $this->Paginator->sort('reg_number') ?></th>
                        <th><?= $this->Paginator->sort('name') ?></th>
                        <th><?= $this->Paginator->sort('gender') ?></th>
                        <th><?= $this->Paginator->sort('grade') ?></th>
                        <th><?= $this->Paginator->sort('sub_grade') ?></th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($students as $student): ?>
                        <tr>
                            <td><?= h($student->reg_number) ?></td>
                            <td><?= h($student->first_name.' '.$student->last_name) ?></td>
                           
                            <td><?= ($student->gender==1)?'Male':'Female'; ?></td>
                            <td><?= ($student->studens_grades[0]->grade->grade) ?></td>
                            <td><?= ($student->studens_grades[0]->sub_grade->sub_grade) ?></td>
                            <td class="actions">

                            <?php echo $this->Html->link($this->Html->tag('span','',array('class'=>'fa fa-eye')), array('action' => 'view', $student->student_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'View This Student','class'=>'btn btn-info btn-icon btn-sm')); ?>
                            
                             <?php echo $this->Html->link($this->Html->tag('span','',array('class'=>'fa fa-edit')), array('action' => 'Edit', $student->student_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Edit This Student','class'=>'btn btn-warning btn-icon btn-sm')); ?>

                             <?php echo $this->Form->postLink($this->Html->tag('span','',array('class'=>'fa fa-trash-o')), array('action' => 'delete', $student->student_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Delete This Grade','class'=>'btn btn-danger btn-icon btn-sm','confirm' => __('Are you sure you want to delete this student?',$student->student_id))); ?>
                             
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
          </section>

    <div class="paginator" align="right">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>