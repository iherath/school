
<section class="vbox">
<section class="scrollable padder">
        <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Students</a></li>
        </ul>

        <div class="m-b-md">
            <h3 class="m-b-none">Students</h3>
        </div>

<section class="panel panel-default">
    <header class="panel-heading">
    <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
    </header>

<div class="row">
    <div class="col-sm-12">
        <section class="panel panel-default">
                <header class="panel-heading font-bold">
                    Create a student
                </header>
        </section>
        <div class="panel-body">
       
         <?= $this->Form->create($student,['enctype'=>'multipart/form-data']) ?>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Student Regiter No</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('reg_number',['class'=>'form-control','required','label'=>false]);?>
                            <br/>
                        </div>
        
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">First Name</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('first_name',['class'=>'form-control','required','label'=>false]);?>
                            <br/>
                        </div>
                  
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Last Name</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('last_name',['class'=>'form-control','required','label'=>false]);?>
                            <br/>
                        </div>
                   
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Date Of Birth</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('date_of_birth',['class'=>'form-control datepicker-input','required','type'=>'text','data-date-format'=>'yyyy-mm-dd','label'=>false]);?>
                            <br/>
                        </div>
                    <br/><br/>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Gender</label>
                        <div class="col-sm-10">
                            <?php echo $this->Form->input('gender', ['options'=>['1'=>'Male','2'=>'Female'], 'empty' => 'Gender','class'=>'form-control','label'=>false ,'required'=>'required','data-error'=>'Please select gender']);?>
                            <br/>
                        </div>
                    <br/><br/>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Contact Number</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('contact_number',['class'=>'form-control','required','label'=>false]);?>
                            <br/>
                        </div>
                   
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Address</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('address',['class'=>'form-control','required','label'=>false]);?>
                            <br/>
                        </div>
                  
                </div>

                <div class="form-group">
                <br/><br/>
                    <label class="col-sm-2 control-label" for="input-id-1">Grade</label>
                    <div class="col-sm-10">
                        <?php  echo $this->Form->input('grade_id',['class'=>'form-control','required','onChange'=>'loadSubGrade()','options'=>$grades,'empty'=>'Please select','label'=>false]);?>
                        <br/>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Medium</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('medium',['class'=>'form-control','required','label'=>false]);?>
                            <br/>
                        </div>
                   
                </div>

                <div class="form-group" id="load-sub-grade">
                <br/><br/>
                    <label class="col-sm-2 control-label" for="input-id-1">Sub Grade</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('sub_grade_id',['class'=>'form-control','required','empty'=>'Please select','label'=>false]);?>
                            <br/>
                        </div>
                    
                </div>
                <br/><br/>
                <div class="form-group">
                <br/><br/>
                    <label class="col-sm-2 control-label" for="input-id-1">Grade</label>
                        <div class="col-sm-5">
                            <table class="table table-striped m-b-none text-sm">
                                  <thead>
                                    <tr>
                                      <th>Subject</th>                    
                                      <th width="70"></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                   <?php 
                                   $firstLimit=round(count($subjects->toArray())/2, 0, PHP_ROUND_HALF_UP);
                                   $i=0;
                                   foreach ($subjects as $subject) {
                                     if($i < $firstLimit){
                                    ?>
                                         <tr>                      
                                          <td><?= $subject->subject_name?></td>
                                          <td class="text-success">
                                            <input type="checkbox" name="subjectId[]" value="<?= $subject->subject_id?>">
                                          </td>
                                        </tr>
                                  <?php
                                       $i++;
                                        }
                                     } ?>
                                  </tbody>
                                </table>
                            <br/>
                        </div>
                        <div class="col-sm-5">
                            <table class="table table-striped m-b-none text-sm">
                                  <thead>
                                    <tr>
                                      <th>Subject</th>                    
                                      <th width="70"></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                <?php $j=0;  
                                   foreach ($subjects as $subject) {
                                     if($j+1>$i){
                                    ?>
                                         <tr>                      
                                           <td><?= $subject->subject_name?></td>
                                          <td class="text-success">
                                             <input type="checkbox" name="subjectId[]" value="<?= $subject->subject_id?>">
                                          </td>
                                        </tr>
                                  <?php
                                        }
                                        $j++;
                                     } ?>
                                  </tbody>
                                </table>
                            <br/>
                        </div>
                </div>
                </div>

                 <div class="form-group">
                       <label class="col-md-2 control-label">Profile Picture(Optional)</label>
                        <div class="col-md-10">
                            <?php  echo $this->Form->input('profile_pic',['type'=>'file','class'=>'form-control','label'=>false ]);?>
                            <span class="file-custom"></span> 
                            </br>    
                        </div>
                    </div>
               <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Description</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('description',['type'=>'textarea','class'=>'form-control','required','label'=>false]);?>
                            <br/>
                        </div>
                   
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1"></label>
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-s-md btn-primary">Create</button>
                             <br/>
                              <br/>
                        </div>
                </div>
                <?= $this->Form->end() ?>
        </div>

    </div>
</div>
</section>
</section>
</section>

