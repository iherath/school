<section class="vbox">
<section class="scrollable padder">
        <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Students</a></li>
        </ul>

        <div class="m-b-md">
            <h3 class="m-b-none">Students</h3>
        </div>
    <section class="panel panel-default">
        <header class="panel-heading">
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
        </header>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                        Form elements
                    </header>
                </section>
            </div>
        </div>

        <div class="panel-body">
        <?= $this->Form->create($mark) ?>


        <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Subject ID</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('subject_id',['class'=>'form-control','label'=>false]);?>
                        </div>
                <br/><br/>
        </div>

        <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Student ID</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('student_id',['class'=>'form-control','label'=>false]);?>
                        </div>
                    <br/><br/>
        </div>
         <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1">Mark</label>
                        <div class="col-sm-10">
                            <?php  echo $this->Form->input('marks',['class'=>'form-control','label'=>false]);?>
                        </div>
                    <br/><br/>
        </div>

        <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-id-1"></label>
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-s-md btn-primary">Edit</button>
                        </div>
        </div>

        <?= $this->Form->end() ?>
        </div>

    </section>
</section>

</section>

