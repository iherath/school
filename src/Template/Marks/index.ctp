<section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Marks</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Marks</h3>
              </div>
              <section class="panel panel-default">
                <header class="panel-heading">
                  DataTables 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
                <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                     
                        <th><?= $this->Paginator->sort('mark_id') ?></th>
                        <th><?= $this->Paginator->sort('subject_id') ?></th>
                        <th><?= $this->Paginator->sort('student_id') ?></th>
                        <th><?= $this->Paginator->sort('marks') ?></th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($marks as $mark): ?>
                        <tr>
                           
                            <td><?= h($mark->mark_id) ?></td>
                            <td><?= h($mark->subject_id) ?></td>
                            <td><?= h($mark->student_id) ?></td>
                            <td><?= h($mark->marks) ?></td>
                            <td class="actions">
                            <?php echo $this->Html->link($this->Html->tag('span','',array('class'=>'fa fa-eye')), array('action' => 'view', $mark->mark_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'View Marks','class'=>'btn btn-info btn-icon btn-sm')); ?>

                                <?php echo $this->Html->link($this->Html->tag('span','',array('class'=>'fa fa-edit')), array('action' => 'Edit', $mark->mark_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Edit Marks','class'=>'btn btn-warning btn-icon btn-sm')); ?>

                                <?php echo $this->Form->postLink($this->Html->tag('span','',array('class'=>'fa fa-trash-o')), array('action' => 'delete', $mark->mark_id), array('escape'=>FALSE,'data-toggle'=>'tooltip','data-placement'=>'top','data-original-title'=>'Delete Marks','class'=>'btn btn-danger btn-icon btn-sm','confirm' => __('Are you sure you want to delete this mark',$mark->mark_id))); ?>

                                
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
          </section>

    <div class="paginator" align="right">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>