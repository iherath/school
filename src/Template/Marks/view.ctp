<section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Marks</a></li>
        
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Marks</h3>
              </div>
              <section class="panel panel-default">
                <header class="panel-heading">
                  DataTables 
                  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
                <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th><?= $this->Paginator->sort('subject_id') ?></th>
                        <th><?= $this->Paginator->sort('student_id') ?></th>
                        <th><?= $this->Paginator->sort('marks') ?></th>
                        
                      </tr>
                    </thead>
                    <tbody>
                  
                        <tr>
                            <td><?= h($mark->subject_id) ?></td>
                            <td><?= h($mark->student_id) ?></td>
                            <td><?= h($mark->marks) ?></td>
                                           
                        
                        </tr>
                  
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
          </section>


