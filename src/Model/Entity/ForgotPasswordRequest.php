<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ForgotPasswordRequest Entity
 *
 * @property int $forgot_password_request_id
 * @property string $user_accesss_domain
 * @property string $email
 * @property \Cake\I18n\Time $date_requested
 * @property \Cake\I18n\Time $date_time
 * @property string $request_code
 * @property int $is_expired
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\ForgotPasswordRequest $forgot_password_request
 */
class ForgotPasswordRequest extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'forgot_password_request_id' => false
    ];
}
