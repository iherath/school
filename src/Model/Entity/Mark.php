<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mark Entity
 *
 * @property int $mark_id
 * @property int $subject_id
 * @property int $student_id
 * @property string $marks
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $is_deleted
 *
 * @property \App\Model\Entity\Mark $mark
 * @property \App\Model\Entity\Subject $subject
 * @property \App\Model\Entity\Student $student
 */
class Mark extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'mark_id' => false
    ];
}
