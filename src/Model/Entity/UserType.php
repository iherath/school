<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserType Entity
 *
 * @property int $user_type_id
 * @property string $user_types
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $is_deleted
 *
 * @property \App\Model\Entity\UserType $user_type
 */
class UserType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'user_type_id' => false
    ];
}
