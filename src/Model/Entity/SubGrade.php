<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubGrade Entity
 *
 * @property int $sub_grades_id
 * @property int $grade_id
 * @property int $student_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $is_deleted
 *
 * @property \App\Model\Entity\SubGrade $sub_grade
 * @property \App\Model\Entity\Grade $grade
 * @property \App\Model\Entity\Student $student
 */
class SubGrade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'sub_grades_id' => false
    ];
}
