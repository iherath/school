<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Log Entity
 *
 * @property int $log_id
 * @property int $generated_by_user_id
 * @property int $generated_by_user_type_id
 * @property string $session_id
 * @property string $subject
 * @property string $message
 * @property string $data
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Log $log
 * @property \App\Model\Entity\GeneratedByUser $generated_by_user
 * @property \App\Model\Entity\GeneratedByUserType $generated_by_user_type
 * @property \App\Model\Entity\Session $session
 */
class Log extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'log_id' => false
    ];
}
