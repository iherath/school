<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ForgotPasswordRequest Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ForgotPasswordRequests
 *
 * @method \App\Model\Entity\ForgotPasswordRequest get($primaryKey, $options = [])
 * @method \App\Model\Entity\ForgotPasswordRequest newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ForgotPasswordRequest[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ForgotPasswordRequest|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ForgotPasswordRequest patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ForgotPasswordRequest[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ForgotPasswordRequest findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ForgotPasswordRequestTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('forgot_password_request');
        $this->displayField('forgot_password_request_id');
        $this->primaryKey('forgot_password_request_id');

        $this->addBehavior('Timestamp');

       
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('user_accesss_domain');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->dateTime('date_requested')
            ->allowEmpty('date_requested');

        $validator
            ->dateTime('date_time')
            ->allowEmpty('date_time');

        $validator
            ->allowEmpty('request_code');

        $validator
            ->integer('is_expired')
            ->allowEmpty('is_expired');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        

        return $rules;
    }
}
