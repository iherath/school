<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StudensGrades Controller
 *
 * @property \App\Model\Table\StudensGradesTable $StudensGrades
 */
class StudensGradesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Students', 'Grades', 'SubGrades']
        ];
        $studensGrades = $this->paginate($this->StudensGrades);

        $this->set(compact('studensGrades'));
        $this->set('_serialize', ['studensGrades']);
    }

    /**
     * View method
     *
     * @param string|null $id Studens Grade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $studensGrade = $this->StudensGrades->get($id, [
            'contain' => ['Students', 'Grades', 'SubGrades']
        ]);

        $this->set('studensGrade', $studensGrade);
        $this->set('_serialize', ['studensGrade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $studensGrade = $this->StudensGrades->newEntity();
        if ($this->request->is('post')) {
            $studensGrade = $this->StudensGrades->patchEntity($studensGrade, $this->request->data);
            if ($this->StudensGrades->save($studensGrade)) {
                $this->Flash->success(__('The studens grade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The studens grade could not be saved. Please, try again.'));
            }
        }
        $students = $this->StudensGrades->Students->find('list', ['limit' => 200]);
        $grades = $this->StudensGrades->Grades->find('list', ['limit' => 200]);
        $subGrades = $this->StudensGrades->SubGrades->find('list', ['limit' => 200]);
        $this->set(compact('studensGrade', 'students', 'grades', 'subGrades'));
        $this->set('_serialize', ['studensGrade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Studens Grade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $studensGrade = $this->StudensGrades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $studensGrade = $this->StudensGrades->patchEntity($studensGrade, $this->request->data);
            if ($this->StudensGrades->save($studensGrade)) {
                $this->Flash->success(__('The studens grade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The studens grade could not be saved. Please, try again.'));
            }
        }
        $students = $this->StudensGrades->Students->find('list', ['limit' => 200]);
        $grades = $this->StudensGrades->Grades->find('list', ['limit' => 200]);
        $subGrades = $this->StudensGrades->SubGrades->find('list', ['limit' => 200]);
        $this->set(compact('studensGrade', 'students', 'grades', 'subGrades'));
        $this->set('_serialize', ['studensGrade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Studens Grade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $studensGrade = $this->StudensGrades->get($id);
        if ($this->StudensGrades->delete($studensGrade)) {
            $this->Flash->success(__('The studens grade has been deleted.'));
        } else {
            $this->Flash->error(__('The studens grade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
