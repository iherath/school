<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ForgotPasswordRequest Controller
 *
 * @property \App\Model\Table\ForgotPasswordRequestTable $ForgotPasswordRequest
 */
class ForgotPasswordRequestController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $forgotPasswordRequest = $this->paginate($this->ForgotPasswordRequest);

        $this->set(compact('forgotPasswordRequest'));
        $this->set('_serialize', ['forgotPasswordRequest']);
    }

    /**
     * View method
     *
     * @param string|null $id Forgot Password Request id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $forgotPasswordRequest = $this->ForgotPasswordRequest->get($id, [
            'contain' => []
        ]);

        $this->set('forgotPasswordRequest', $forgotPasswordRequest);
        $this->set('_serialize', ['forgotPasswordRequest']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $forgotPasswordRequest = $this->ForgotPasswordRequest->newEntity();
        if ($this->request->is('post')) {
            $forgotPasswordRequest = $this->ForgotPasswordRequest->patchEntity($forgotPasswordRequest, $this->request->data);
            if ($this->ForgotPasswordRequest->save($forgotPasswordRequest)) {
                $this->Flash->success(__('The forgot password request has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The forgot password request could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('forgotPasswordRequest'));
        $this->set('_serialize', ['forgotPasswordRequest']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Forgot Password Request id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $forgotPasswordRequest = $this->ForgotPasswordRequest->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $forgotPasswordRequest = $this->ForgotPasswordRequest->patchEntity($forgotPasswordRequest, $this->request->data);
            if ($this->ForgotPasswordRequest->save($forgotPasswordRequest)) {
                $this->Flash->success(__('The forgot password request has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The forgot password request could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('forgotPasswordRequest'));
        $this->set('_serialize', ['forgotPasswordRequest']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Forgot Password Request id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $forgotPasswordRequest = $this->ForgotPasswordRequest->get($id);
        if ($this->ForgotPasswordRequest->delete($forgotPasswordRequest)) {
            $this->Flash->success(__('The forgot password request has been deleted.'));
        } else {
            $this->Flash->error(__('The forgot password request could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
