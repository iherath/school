<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SudentSubjects Controller
 *
 * @property \App\Model\Table\SudentSubjectsTable $SudentSubjects
 */
class SudentSubjectsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Students', 'Subjects']
        ];
        $sudentSubjects = $this->paginate($this->SudentSubjects);

        $this->set(compact('sudentSubjects'));
        $this->set('_serialize', ['sudentSubjects']);
    }

    /**
     * View method
     *
     * @param string|null $id Sudent Subject id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sudentSubject = $this->SudentSubjects->get($id, [
            'contain' => ['Students', 'Subjects']
        ]);

        $this->set('sudentSubject', $sudentSubject);
        $this->set('_serialize', ['sudentSubject']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sudentSubject = $this->SudentSubjects->newEntity();
        if ($this->request->is('post')) {
            $sudentSubject = $this->SudentSubjects->patchEntity($sudentSubject, $this->request->data);
            if ($this->SudentSubjects->save($sudentSubject)) {
                $this->Flash->success(__('The sudent subject has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sudent subject could not be saved. Please, try again.'));
            }
        }
        $students = $this->SudentSubjects->Students->find('list', ['limit' => 200]);
        $subjects = $this->SudentSubjects->Subjects->find('list', ['limit' => 200]);
        $this->set(compact('sudentSubject', 'students', 'subjects'));
        $this->set('_serialize', ['sudentSubject']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sudent Subject id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sudentSubject = $this->SudentSubjects->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sudentSubject = $this->SudentSubjects->patchEntity($sudentSubject, $this->request->data);
            if ($this->SudentSubjects->save($sudentSubject)) {
                $this->Flash->success(__('The sudent subject has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sudent subject could not be saved. Please, try again.'));
            }
        }
        $students = $this->SudentSubjects->Students->find('list', ['limit' => 200]);
        $subjects = $this->SudentSubjects->Subjects->find('list', ['limit' => 200]);
        $this->set(compact('sudentSubject', 'students', 'subjects'));
        $this->set('_serialize', ['sudentSubject']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sudent Subject id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sudentSubject = $this->SudentSubjects->get($id);
        if ($this->SudentSubjects->delete($sudentSubject)) {
            $this->Flash->success(__('The sudent subject has been deleted.'));
        } else {
            $this->Flash->error(__('The sudent subject could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
