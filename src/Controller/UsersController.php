<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function isAuthorized($user) {
        return true;
    }

    public function login(){
        $this->loadModel('Logs');
        $this->request->session()->start();
        $session_id =$this->request->session()->id(); 
        $this->viewBuilder()->layout('login');

        if($this->Auth->user('user_id') || $this->Cookie->read('user.user_id')){
            
            if($this->Cookie->read('user.user_id') !=''){
                $this->Auth->setUser($this->Cookie->read('user'));
            }
            return $this->redirect($this->Auth->redirectUrl());

        }else{
             if ($this->request->is('post')) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    if(isset($this->request->data['remember_me'])){
                        $this->Cookie->write('user', $user);
                    }
                    
                     //add log entry
                    // $this->Logs->addEntry(Configure::read('Logs.Admin.login'),
                    //     $session_id,
                    //     array(
                    //             $user['title'].' '.$user['first_name'].' '.$user['last_name'] 
                    //     )
                    //     ,
                    //     json_encode(array(
                    //             'browser' => $_SERVER['HTTP_USER_AGENT']
                    //     )),
                    //     $user['user_type_id'],
                    //     $user['user_id'],
                    //     null,
                    //     'Login'
                    //     );

                    return $this->redirect($this->Auth->redirectUrl());
                 } else {

                    $this->Flash->error(
                        __('Username or password is incorrect'),
                        'default',
                        [],
                        'auth'
                    );
                }
            }
        }
    }

    /* admin login method logout
    *
    */
    public function logout()
    {   
        $this->loadModel('Logs');
        $session_id =$this->request->session()->id(); 
        $this->Flash->success(__('Logged out.'));
        $userID=$this->Auth->user('user_id');
        $user = $this->Users->get($userID);
        $this->Cookie->delete('user');
        // $this->Logs->addEntry(Configure::read('Logs.Admin.logout'),
        //             $session_id,
        //             array(
        //                     $user['title'].' '.$user['first_name'].' '.$user['last_name']
        //             )
        //             ,
        //             json_encode(array(
        //                     'browser' => $_SERVER['HTTP_USER_AGENT']
        //             )),
        //             $user['user_type_id'],
        //             $user['user_id'],
        //             null,
        //             'Logout'
        //             );
        return $this->redirect($this->Auth->logout());
    }
    

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['UserTypes']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserTypes']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

       /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Logs');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
           
            if($this->request->data['password']==$this->request->data['confirm_password']){
                $this->request->data['password']=(new DefaultPasswordHasher)->hash($this->request->data['password']); 
                $user = $this->Users->patchEntity($user, $this->request->data);
                $target_path = "profile_pic/";
                if(!empty($this->request->data['profile_pic']['name'])){
                    $newFileName=rand(1,99999).basename($this->request->data['profile_pic']['name']);
                    $target_path = $target_path .$newFileName;
                    if(move_uploaded_file($this->request->data['profile_pic']['tmp_name'], $target_path)){
                        $user->profile_pic=$newFileName;
                        if ($this->Users->save($user)) {
                            $this->Flash->success(__('The user has been created.'));
                            //add log entry
                            // $userID=$this->Auth->user('user_id');
                            // $logedUser = $this->Users->get($userID);
                            // $this->request->session()->start();
                            // $session_id =$this->request->session()->id();     
                            // $this->Logs->addEntry(Configure::read('Logs.Admin.CreateANewUserProfile'),
                            //     $session_id,
                            //     array(
                            //             $logedUser['title'].' '.$logedUser['first_name'].' '.$logedUser['last_name'],
                            //             $user->title.' '.$user->first_name.' '.$user->last_name
                            //     )
                            //     ,
                            //     json_encode(array(
                            //             'browser' => $_SERVER['HTTP_USER_AGENT']
                            //     )),
                            //     $logedUser['user_type_id'],
                            //     $logedUser['user_id'],
                            //     null,
                            //     'Admin User Profile Create'
                            //     );

                         return $this->redirect(['action' => 'index']);
                        } else {
                            $this->Flash->error(__('The user could not be saved. Please, try again.'));
                        }
                    }else{
                         $this->Flash->error(__('There was an error uploading the image. Please, try again.'));
                    } 
                }else{
                    if ($this->Users->save($user)) {
                         $this->Flash->success(__('The user has been saved.'));
                         //add log entry
                            $userID=$this->Auth->user('user_id');
                            $logedUser = $this->Users->get($userID);
                            $this->request->session()->start();
                            $session_id =$this->request->session()->id();     
                            $this->Logs->addEntry(Configure::read('Logs.Admin.CreateANewUserProfile'),
                                $session_id,
                                array(
                                        $logedUser['title'].' '.$logedUser['first_name'].' '.$logedUser['last_name'],
                                        $user->title.' '.$user->first_name.' '.$user->last_name
                                )
                                ,
                                json_encode(array(
                                        'browser' => $_SERVER['HTTP_USER_AGENT']
                                )),
                                $logedUser['user_type_id'],
                                $logedUser['user_id'],
                                null,
                                'Admin User Profile Create'
                                );

                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('The user could not be saved. Please, try again.'));
                    }
                }
            }else{
                $this->Flash->error(__('Please check your confirm password and try again.'));
            }

        }
        
         $userTypes = $this->Users->UserTypes->find('list', ['keyField'=>'user_type_id','valueField'=>'user_types','conditions'=>['UserTypes.is_deleted'=>0],'limit' => 200]);
        $this->set(compact('user', 'userTypes'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
         $this->loadModel('Logs');
         
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

             $target_path = "profile_pic/";
             
            if(!empty($this->request->data['profile_pic']['name'])){

                $newFileName=rand(1,99999).basename($this->request->data['profile_pic']['name']);
                $target_path = $target_path .$newFileName;
                if(move_uploaded_file($this->request->data['profile_pic']['tmp_name'], $target_path)){
                    $user->profile_pic=$newFileName;
                    if ($this->Users->save($user)) {
                     $this->Flash->success(__('The user has been updated.'));
                     return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('The user could not be updated. Please, try again.'));
                    }
                }else{
                     $this->Flash->error(__('There was an error uploading the image. Please, try again.'));
                } 

             }else{
                $this->request->data['profile_pic']=$user->profile_pic;
                $user = $this->Users->patchEntity($user, $this->request->data);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The user has been updated.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The user could not be updated. Please, try again.'));
                }
             }   
        }
    
        $userTypes = $this->Users->UserTypes->find('list', ['keyField'=>'user_type_id','valueField'=>'user_types','conditions'=>['UserTypes.is_deleted'=>0],'limit' => 200]);
        $this->set(compact('user', 'userTypes'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
