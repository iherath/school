<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Controller\Component\CookieComponent;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');
        $this->loadModel('Users');
        $this->Cookie->config('path', '/');
        $this->Cookie->config([
                'expires' => '+10 days',
                'httpOnly' => true
            ]);

        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
                
            ],
            'loginRedirect' => [
                    'controller' => 'Users',
                    'action' => 'dashboard'
                ],
            'authError' => 'Did you really think you are allowed to see that?',
            'authorize' => array('Controller'), // <- here
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username','password'=>'password']
                ]
            ]
         ]);


       
        $userID=$this->Auth->user('user_id');
       
        if(isset($userID)){
             $logedUser = $this->Users->get($userID,['contain'=>['UserTypes']]);
        }else{
           $logedUser=NULL; 
           
        }
              
        $this->set('logedUser',$logedUser);       
      
    }


    public function isAuthorized($user) {
        return true;
    }
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
