<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Students Controller
 *
 * @property \App\Model\Table\StudentsTable $Students
 */
class StudentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SudentSubjects','StudensGrades'=>['Grades','SubGrades']]
            ,'limit'=>20
        ];
        
        $students = $this->paginate($this->Students);

        $this->set(compact('students'));
        $this->set('_serialize', ['students']);
    }

    /**
     * View method
     *
     * @param string|null $id Student id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $student = $this->Students->get($id, [
            'contain' => ['SudentSubjects'=>['Subjects'],'StudensGrades'=>['Grades','SubGrades']]
        ]);

        $this->set('student', $student);
        $this->set('_serialize', ['student']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Grades');
        $this->loadModel('Subjects');
        $this->loadModel('SudentSubjects');
        $this->loadModel('StudensGrades');

        $student = $this->Students->newEntity();
        if ($this->request->is('post')) {

            $student = $this->Students->patchEntity($student, $this->request->data);
            $target_path = "student_pic/";
            if(!empty($this->request->data['profile_pic']['name'])){
                    $newFileName=rand(1,99999).basename($this->request->data['profile_pic']['name']);
                    $target_path = $target_path .$newFileName;
                    if(move_uploaded_file($this->request->data['profile_pic']['tmp_name'], $target_path)){
                        $student->profile_pic=$newFileName;
                        if ($this->Students->save($student)) {

                            $studensGrade = $this->StudensGrades->newEntity();
                            $studensGrade->student_id=$student->student_id;
                            $studensGrade->grade_id=$this->request->data['grade_id'];
                            $studensGrade->sub_grades_id=$this->request->data['sub_grade_id'];
                            $this->StudensGrades->save($studensGrade);

                            foreach ($this->request->data['subjectId'] as $subjectId) {
                                 $sudentSubject = $this->SudentSubjects->newEntity();
                                 $sudentSubject->sudent_id=$student->student_id;
                                 $sudentSubject->subject_id= $subjectId;
                                 $this->SudentSubjects->save($sudentSubject);
                            }

                            $this->Flash->success(__('The student has been saved.'));

                            return $this->redirect(['action' => 'index']);
                        } else {
                            $this->Flash->error(__('The student could not be saved. Please, try again.'));
                        }
                    }else{
                         $this->Flash->error(__('There was an error uploading the image. Please, try again.'));
                    } 
                }else{

                    if ($this->Students->save($student)) {
                            $studensGrade = $this->StudensGrades->newEntity();
                            $studensGrade->student_id=$student->student_id;
                            $studensGrade->grade_id=$this->request->data['grade_id'];
                            $studensGrade->sub_grades_id=$this->request->data['sub_grade_id'];
                            $this->StudensGrades->save($studensGrade);
                            foreach ($this->request->data['subjectId'] as $subjectId) {
                                 $sudentSubject = $this->SudentSubjects->newEntity();
                                 $sudentSubject->sudent_id=$student->student_id;
                                 $sudentSubject->subject_id= $subjectId;
                                 $this->SudentSubjects->save($sudentSubject);
                            }

                        $this->Flash->success(__('The student has been saved.'));

                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('The student could not be saved. Please, try again.'));
                    }
            }
        }
        $grades = $this->Grades->find('list', ['keyField'=>'grade_id','valueField'=>'grade','conditions'=>['Grades.is_deleted'=>0],'order'=>'Grades.grade ASC','limit' => 200]);
        $subjects = $this->Subjects->find('all', ['conditions'=>['Subjects.is_deleted'=>0]]);
        $this->set(compact('student','grades','subjects'));
        $this->set('_serialize', ['student']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Student id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Grades');
        $this->loadModel('Subjects');
        $this->loadModel('SudentSubjects');
        $this->loadModel('StudensGrades');
        $this->loadModel('SubGrades');
        $student = $this->Students->get($id, [
            'contain' => ['SudentSubjects','StudensGrades']
        ]);

      
        if ($this->request->is(['patch', 'post', 'put'])) {
            $student = $this->Students->patchEntity($student, $this->request->data);
            $target_path = "student_pic/";
            if(!empty($this->request->data['profile_pic']['name'])){
                    $newFileName=rand(1,99999).basename($this->request->data['profile_pic']['name']);
                    $target_path = $target_path .$newFileName;
                    if(move_uploaded_file($this->request->data['profile_pic']['tmp_name'], $target_path)){
                        $student->profile_pic=$newFileName;
                        if ($this->Students->save($student)) {

                            foreach ($student->studens_grades as $studens_grade) {
                               $studentGrade=$this->StudensGrades->get($studens_grade->student_grades_id);
                       
                               $this->StudensGrades->delete($studentGrade);
                            }


                            $studensGrade = $this->StudensGrades->newEntity();
                            $studensGrade->student_id=$student->student_id;
                            $studensGrade->grade_id=$this->request->data['grade_id'];
                            $studensGrade->sub_grades_id=$this->request->data['sub_grade_id'];
                            $this->StudensGrades->save($studensGrade);

                            foreach ($student->sudent_subjects as $sudent_subjects) {
                               $sudentSubject=$this->SudentSubjects->get($sudent_subjects->sudent_subject_id);
                               $this->SudentSubjects->delete($sudentSubject);
                            }

                            foreach ($this->request->data['subjectId'] as $subjectId) {
                                 $sudentSubject = $this->SudentSubjects->newEntity();
                                 $sudentSubject->student_id=$student->student_id;
                                 $sudentSubject->subject_id= $subjectId;
                                 $this->SudentSubjects->save($sudentSubject);
                            }

                            $this->Flash->success(__('The student has been updated.'));

                            return $this->redirect(['action' => 'index']);
                        } else {
                            $this->Flash->error(__('The student could not be updated. Please, try again.'));
                        }
                    }else{
                         $this->Flash->error(__('There was an error uploading the image. Please, try again.'));
                    } 
                }else{
                    $student->profile_pic=$student->profile_pic;
                    if ($this->Students->save($student)) {
                            foreach ($student->studens_grades as $studens_grade) {
                               $studentGrade=$this->StudensGrades->get($studens_grade->student_grades_id);
                             
                               $this->StudensGrades->delete($studentGrade);
                            }

                            $studensGrade = $this->StudensGrades->newEntity();
                            $studensGrade->student_id=$student->student_id;
                            $studensGrade->grade_id=$this->request->data['grade_id'];
                            $studensGrade->sub_grades_id=$this->request->data['sub_grade_id'];
                            $this->StudensGrades->save($studensGrade);

                            foreach ($student->sudent_subjects as $sudent_subjects) {
                               $sudentSubject=$this->SudentSubjects->get($sudent_subjects->sudent_subject_id);
                               $this->SudentSubjects->delete($sudentSubject);
                            }



                            foreach ($this->request->data['subjectId'] as $subjectId) {

                              
                                 $sudentSubject = $this->SudentSubjects->newEntity();
                                 $sudentSubject->student_id=$student->student_id;
                                 $sudentSubject->subject_id= $subjectId;
                                 $this->SudentSubjects->save($sudentSubject);


                            }

                        $this->Flash->success(__('The student has been updated.'));

                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('The student could not be updated. Please, try again.'));
                    }
            } $this->Flash->error(__('The student could not be updated. Please, try again.'));
        }
        


        $grades = $this->Grades->find('list', ['keyField'=>'grade_id','valueField'=>'grade','conditions'=>['Grades.is_deleted'=>0],'order'=>'Grades.grade ASC','limit' => 200]);
        $subGrades = $this->SubGrades->find('list', ['keyField'=>'sub_grades_id','valueField'=>'sub_grade','conditions'=>['SubGrades.is_deleted'=>0,'SubGrades.grade_id'=>$student->studens_grades[0]->grade_id]]);
        $subjects = $this->Subjects->find('all', ['conditions'=>['Subjects.is_deleted'=>0]]);

        $sudentSubjects=array();

        $i=0;
        foreach ($subjects as $subject) {
            $sudentSubjects[$i]['checked']= 0;
            $sudentSubjects[$i]['subject']= $subject->subject_name;
            $sudentSubjects[$i]['subject_id']= $subject->subject_id;
            foreach ($student->sudent_subjects as $sudent_subject) {

                if($subject->subject_id==$sudent_subject->subject_id){
                    
                    $sudentSubjects[$i]['checked']= 1;
                  
                    
                } 
                
            }
            $i++;
        }    

     


        $this->set(compact('student','grades','sudentSubjects','subGrades'));
        $this->set('_serialize', ['student']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Student id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $student = $this->Students->get($id);
        if ($this->Students->delete($student)) {
            $this->Flash->success(__('The student has been deleted.'));
        } else {
            $this->Flash->error(__('The student could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
