<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SubGrades Controller
 *
 * @property \App\Model\Table\SubGradesTable $SubGrades
 */
class SubGradesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Grades', 'Students']
        ];
        $subGrades = $this->paginate($this->SubGrades);

        $this->set(compact('subGrades'));
        $this->set('_serialize', ['subGrades']);
    }

    /**
     * View method
     *
     * @param string|null $id Sub Grade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subGrade = $this->SubGrades->get($id, [
            'contain' => ['Grades', 'Students']
        ]);

        $this->set('subGrade', $subGrade);
        $this->set('_serialize', ['subGrade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subGrade = $this->SubGrades->newEntity();
        if ($this->request->is('post')) {
            $subGrade = $this->SubGrades->patchEntity($subGrade, $this->request->data);
            if ($this->SubGrades->save($subGrade)) {
                $this->Flash->success(__('The sub grade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sub grade could not be saved. Please, try again.'));
            }
        }
        $grades = $this->SubGrades->Grades->find('list', ['keyField'=>'grade_id','valueField'=>'grade','limit' => 200]);
        $students = $this->SubGrades->Students->find('list', ['limit' => 200]);
        $this->set(compact('subGrade', 'grades', 'students'));
        $this->set('_serialize', ['subGrade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sub Grade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subGrade = $this->SubGrades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subGrade = $this->SubGrades->patchEntity($subGrade, $this->request->data);
            if ($this->SubGrades->save($subGrade)) {
                $this->Flash->success(__('The sub grade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sub grade could not be saved. Please, try again.'));
                
            }
        }
        $grades = $this->SubGrades->Grades->find('list', ['keyField'=>'grade_id','valueField'=>'grade','limit' => 200]);
        $students = $this->SubGrades->Students->find('list', ['limit' => 200]);
        $this->set(compact('subGrade', 'grades', 'students'));
        $this->set('_serialize', ['subGrade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sub Grade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subGrade = $this->SubGrades->get($id);
        if ($this->SubGrades->delete($subGrade)) {
            $this->Flash->success(__('The sub grade has been deleted.'));
        } else {
            $this->Flash->error(__('The sub grade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getSubGrades(){
         $subGrades = $this->SubGrades->find('list', ['keyField'=>'sub_grades_id','valueField'=>'sub_grade','conditions'=>['SubGrades.grade_id'=>$this->request->data['gradeId']],'limit' => 200]);
         $this->set(compact('subGrades'));
    }
}
