<?php
use Cake\Core\Configure;
define('APP_URL','http://localhost/school/');
Configure::write('Logs.Admin.login', "The admin user %0% has been logged in to the system");
Configure::write('Logs.Admin.logout', "The admin user %0% has been logout from the system");
Configure::write('Logs.Admin.CreateANewVehicelProfile', "The admin user %0% has been created new %1% vehicle profile for %2% Membership ID");
Configure::write('Logs.Admin.UpdateVehicelProfile', "The admin user %0% has been updated vehicle profile with %1% Membership ID");
Configure::write('Logs.Admin.CreateANewUserProfile', "The admin user %0% has been created a new user profile for %1%");
Configure::write('Logs.Admin.UpdateSOSStatus', "The admin user %0% has been updated status to %1% for SOS NO %2%");
Configure::write('Logs.Admin.AllocatAgentForSOS', "The admin user %0% has been allocated service agent %1% from %2% for SOS NO %3%");
Configure::write('Logs.Admin.SendInvoiceToCustomer', "The admin user %0% has been sent Invoice - %1% for %2% SOS request" );
Configure::write('Logs.Message.reachedPayNow', "Customer has reached the pay now page for session id '%0%' with payment '%1%'");

///////////////////////////////////////////////////////////////////////////////////

Configure::write('Email.AdminEmails',['isha.niranga@gmail.com']);
Configure::write('Email.FromAddress',['ishan.herath@benworldwide.com']);
Configure::write('Project.Name','DIMO-Pick My Load');

Configure::write('SECURITY_SALT','3aadc13b52ca88f96f709f003b37588b7eb61a853b02dad181503e7f547c23c9');


//////////////////////// Notification Types //////////////////////////////////////

define('ADMIN_NOTIFICATION',1);
define('CUSTOMER_NOTIFICATION',2);
define('DRIVER_NOTIFICATION',3);



