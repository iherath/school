set :stage, :staging

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
#role :app, %w{deploy.benww.com}
#role :db,  %w{deploy@example.com}
role :app, "deploy.bww.es", :primary => true 

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server
# definition into the server list. The second argument
# something that quacks like a hash can be used to set
# extended properties on the server.
#server 'deploy.benww.com', user: 'deploybww', roles: [:app]

# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
# set :ssh_options, {
#   #keys: %w(/home/rlisowski/.ssh/id_rsa),
#   forward_agent: true,
#   auth_methods: %w(password),
#   port: 2222
# }
# and/or per server
server 'deploy.bww.es',
  user: 'deploybww',
  password: '.uj0tnmakEhlhO1W',
  roles: %w{app},
  ssh_options: {
    # user: 'deploybww', # overrides user setting above
    #keys: %w(/home/user_name/.ssh/id_rsa),
    forward_agent: true,

    # auth_methods: %w(publickey password),
    port: 22
    # password: 'please use keys'
  },
    default_run_options:{

    pty:true
  }

# setting per server overrides global ssh_options

# fetch(:default_env).merge!(rails_env: :staging)

set :branch, 'master'

set :deploy_to, "/home/deploybww/public_html/school"

set :use_sudo, false

set :deploy_via, :remote_cache
set :copy_exclude, [".git", ".DS_Store", ".gitignore", ".gitmodules", "Capfile", "config/*"]

#default_run_options[:pty] = true

set :group_writable, false #fixes mkdir permissions
set :port, 22 #ssh port
#set :ssh_options, {:forward_agent => true}

#set :keep_releases, 3

set :tmp_dir, '/home/deploybww/public_html/school/tmp'


namespace :t do
  desc 'something'
  task :setup_host_files do
    on roles(:all) do 
      execute "cp /home/deploybww/public_html/school/default/app.php #{fetch(:release_path)}/config/;cp /home/deploybww/public_html/school/default/configuration.php #{fetch(:release_path)}/config/app_config/;cp /home/deploybww/public_html/school/default/bootstrap.php #{fetch(:release_path)}/config/";
    end
  end
end


namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after :finishing, 'deploy:cleanup'
  after :updated, "t:setup_host_files" #copy the server specific config files to its place



end
